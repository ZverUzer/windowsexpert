# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Expert system for Windows boot failure diagnostic based on brain.js and angular.js

### Installation ###

npm install -g yo  
npm install -g generator-angular-material

npm install  
bower install

### Run server ###

gulp serve

### Run dist server ###

gulp serve:dist

compresses js / css / images
all JavaScript is generated anonymous

### Run test server ###

gulp serve:spec

### Run tests ###

gulp spec

### Generate route ###

yo angular-material:route routeName
