(function (app) {
    app.controller('AboutController', ['$scope', AboutController]);

    function AboutController($scope) {
        $scope.title = "Output from brain.js: ";
    }
})(WindowsExpert);
