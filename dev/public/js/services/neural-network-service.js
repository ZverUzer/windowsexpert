(function (app) {
    app.service('NeuralNetworkService', ['', NeuralNetworkService]);

    function NeuralNetworkService() {
        var net = new brain.NeuralNetwork();

        net.train([{input: [0, 0], output: [0]},
            {input: [0, 1], output: [1]},
            {input: [1, 0], output: [1]},
            {input: [1, 1], output: [0]}]);

        return {
            run: run
        };

        function run(args) {
            return net.run(args);
        }
    }
})(WindowsExpert);
