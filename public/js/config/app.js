var WindowsExpert = angular.module('WindowsExpert', ['ngMaterial', 'ngAnimate', 'ngMessages', 'ngAria', 'ui.router']);

(function (app) {
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider.state('powersupply', {
            url: '/powersupply',
            templateUrl: 'partials/power-supply-partial.html',
            controller: 'powersupplyController'
        })

            .state('home', {
                url: '/',
                templateUrl: 'partials/home-partial.html',
                controller: 'HomeController'
            })

            .state('about', {
                url: '/about',
                templateUrl: 'partials/about-partial.html',
                controller: 'AboutController'
            })

            .state('power', {
                url: '/power',
                templateUrl: 'partials/power-supply-partial.html',
                controller: 'PowerSupplyController'
            });
    }]);
})(WindowsExpert);
