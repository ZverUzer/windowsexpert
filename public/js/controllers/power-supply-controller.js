(function (app) {
    app.controller('PowerSupplyController', ['$scope', '$timeout', function ($scope, $timeout) {
        $scope.busy = true;
        $scope.onAnswer = onAnswer;
        $scope.reset = reset;

        $timeout(function () {
            $scope.answers = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            prepareNetwork();
            $scope.busy = false;

            $scope.questionIndex = 0;
        }, 700);

        function onAnswer(answer) {
            if ($scope.questionIndex < $scope.questions.length - 1) {
                $scope.answers[$scope.questionIndex] = answer;
                $scope.questionIndex++;
            } else {
                showResults();
            }
        }

        function showResults() {
            checkResults(0.5);
        }

        function reset() {
            $scope.suggestions = null;
            $scope.questionIndex = 0;
        }

        function checkResults(rate) {
            $scope.busy = true;

            $timeout(function () {
                $scope.suggestions = [];
                var results = $scope.net.run($scope.answers);

                for (var suggestion in results) {
                    if (results[suggestion] > rate) {
                        $scope.suggestions.push($scope.answerMap.get(suggestion));
                    }
                }

                if(suggestion.length === 0) {
                    suggestion = null;
                }

                $scope.busy = false;
            }, 50);
        }

        function prepareNetwork() {
            $scope.net = new brain.NeuralNetwork();

            $scope.answerMap = new Map();
            $scope.answerMap.set("checkPowerSupplyRate", "Проверьте, достаточно ли питания подаётся на элементы.");
            $scope.answerMap.set("prematurePowerGoodSignal", "Преждевременный сигнал POWER_GOOD, следует заменить блок питания.");
            $scope.answerMap.set("motherboardFailure", "Неисправность материнской платы, перейти к диагностике.");
            $scope.answerMap.set("removeNewComponents", "Новое оборудование установлено некорректно, удалите его.");
            $scope.answerMap.set("cleanOrReplaceCooler", "Вентилятор следует хотя бы изредка чистить...");
            $scope.answerMap.set("checkPowerSupplyStability", "Проверьте блок питания, ваш работает нестабильно.");
            $scope.answerMap.set("hardDriveDiskFailure", "Неисправность жёсткого диска, перейти к диагностике.");
            $scope.answerMap.set("checkPowerSocket", "Проверьте, пожалуйста, исправность розетки.");
            $scope.answerMap.set("checkPowerSupplyVoltage", "Выставите правильное напряжение на задней стенке БП.");
            $scope.answerMap.set("checkPowerCable", "Убедитесь в правильности подключения кабеля кнопки питания к плате.");
            $scope.answerMap.set("changePowerSwitch", "Замените выключатель питания, попробуйте воспользоваться кнопкой reset.");
            $scope.answerMap.set("recheckPowerSupplyCable", "Переподключите кабели блока питания к материнской плате.");
            $scope.answerMap.set("powerSupplyDefect", "Дефект блока питания, проверьте на другом ПК и если не заработает - выбросьте и купи новый.");
            $scope.answerMap.set("checkHardDriveDisk", "Проверьте жёствий диск на тестовом ПК или используйте стопроцентно рабочий.");
            $scope.answerMap.set("removeNewComponentsExceptVideo", "Удалите дополнительное оборудование из ПК, оставьте только видеокарту, перейти к диагностике конфликтов оборудования.");
            $scope.answerMap.set("motherboardClosure", "Замыкание материнской платы на корпус ПК, плохая жёсткость корпуса либо он слишком кривой. Возможно плохое соединение видеокарты с материнской платой из-за деформированного корпуса.");
            $scope.answerMap.set("checkOrChangePowerSupply", "Проверить или заменить БП.");

            $scope.questions = [
                "Блок питания включается?",
                "Монитор показывает что-нибудь?",
                "Новая сборка?",
                "Запускается ли со второй попытки?",
                "Слышно более одного писка?",
                "Установлено новое оборудование?",
                "Громкий шум вентилятора?",
                "Отображаются ошибки жёсткого диска или ОС?",
                "Проверена ли розетка для БП?",
                "БП имеет универсальный вход?",
                "Кабель от кнопки питания проверен?",
                "Выключатель питания может быть неисправен?",
                "Блок питания правильно подключен к материнской плате?",
                "Жёсткий диск запускается?",
                "Жёсткий диск точно исправен?",
                "Плохой адаптер на шине?",
                "Присутствует напряжение на материнской плате?",
            ];

            var trainSet = [
                {input: [1, 1, 1, 0, 0, 0, 0, 0, 0], output: {checkPowerSupplyRate: 1}},
                {input: [0.9, 0.9, 0.9, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1], output: {checkPowerSupplyRate: 1}},
                {input: [0.8, 0.8, 0.8, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2], output: {checkPowerSupplyRate: 1}},
                {input: [0.7, 0.7, 0.7, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3], output: {checkPowerSupplyRate: 1}},
                {input: [0.6, 0.6, 0.6, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4], output: {checkPowerSupplyRate: 1}},
                {input: [1, 1, 0, 1, 0, 0, 0, 0, 0], output: {prematurePowerGoodSignal: 1}},
                {input: [0.9, 0.9, 0.1, 0.9, 0.1, 0.1, 0.1, 0.1, 0.1], output: {prematurePowerGoodSignal: 1}},
                {input: [0.8, 0.8, 0.2, 0.8, 0.2, 0.2, 0.2, 0.2, 0.2], output: {prematurePowerGoodSignal: 1}},
                {input: [0.7, 0.7, 0.3, 0.7, 0.3, 0.3, 0.3, 0.3, 0.3], output: {prematurePowerGoodSignal: 1}},
                {input: [0.6, 0.6, 0.4, 0.6, 0.4, 0.4, 0.4, 0.4, 0.4], output: {prematurePowerGoodSignal: 1}},
                {input: [1, 1, 0, 0, 1, 0, 0, 0, 0], output: {motherboardFailure: 1}},
                {input: [1, 1, 0, 0, 0, 1, 0, 0, 0], output: {removeNewComponents: 1}},
                {input: [1, 1, 0, 0, 0, 0, 1, 0, 0], output: {cleanOrReplaceCooler: 1}},
                {input: [1, 1, 0, 0, 0, 0, 0, 0, 0], output: {checkPowerSupplyStability: 1}},
                {input: [1, 1, 0, 0, 0, 0, 0, 1, 0], output: {hardDriveDiskFailure: 1}},
                {input: [0, 0, 0, 0, 0, 0, 0, 0, 0], output: {checkPowerSocket: 1}},
                {input: [0, 1, 0, 0, 0, 0, 0, 0, 0], output: {checkPowerSupplyVoltage: 1}},
                {input: [0, 1, 1, 0, 0, 0, 0, 0, 0], output: {checkPowerCable: 1}},
                {input: [0, 1, 1, 1, 1, 0, 0, 0, 0], output: {changePowerSwitch: 1}},
                {input: [0, 1, 1, 1, 0, 0, 0, 0, 0], output: {recheckPowerSupplyCable: 1}},
                {input: [0, 1, 1, 1, 0, 1, 0, 1, 0], output: {powerSupplyDefect: 1}},
                {input: [0, 1, 1, 1, 0, 1, 0, 0, 0], output: {checkHardDriveDisk: 1}},
                {input: [0, 1, 1, 1, 0, 1, 1, 1, 0], output: {removeNewComponentsExceptVideo: 1}},
                {input: [0, 1, 1, 1, 0, 1, 1, 0, 1], output: {motherboardClosure: 1}},
                {input: [0, 1, 1, 1, 0, 1, 1, 0, 0], output: {checkOrChangePowerSupply: 1}}
            ];

            $scope.net.train(trainSet);
        }
    }]);
})(WindowsExpert);
